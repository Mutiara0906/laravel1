<!DOCTYPE html>
<html>
<head>
	<title>Halaman Kedua </title>
	<meta charset="UTF-8">
</head>
<body>
<div>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
</div>
<form action="/Submit" method ="post">
{{ csrf_field() }}
	<label for= "first_name">First Name : </label>
	<br><input type ="text" id="first_name" value = "{{old('namaDepan')}}"><br>
	<br><label for= "last_name">Last Name : </label>
	<br><input type ="text" id="last_name" value = "{{old('namaBelakang')}}"><br>
	<br>
	<label>Gender:</label><br>
	<input type= "radio" name = "gender" value ="0">Male<br>
	<input type= "radio" name = "gender" value ="1">Female<br>
	<input type= "radio" name = "gender" value ="2">Other<br>
	<br>
	<label>Nationally:</label><br>
	<select>
		<option value = "0">Dustch</option>
		<option value = "1">Indonesian</option>
		<option value = "2">Australian</option>
	</select>
	<br>
	<br>
	<label>Language Spoken:</label><br>
	<input type="checkbox" name = "language" value ="0">Bahasa Indonesia <br>
	<input type="checkbox" name = "language" value ="1">English <br>
	<input type="checkbox" name = "language" value ="2">Other <br>
	<br>
	<label for="bio">Bio:</label><br>
	<textarea cols ="25" rows = "10" id ="bio"></textarea>
	<br> 
	<input type ="submit" value = "Sign Up">
</form>
</body>
</html>