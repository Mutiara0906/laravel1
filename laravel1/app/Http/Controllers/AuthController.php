<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signup(){
        return view("SignUp");
    }

    public function submit( Request $request){
        $this -> validate($request,[
            'namaDepan' => 'required',
            'namaBelakang' => 'required'
        ]);

        return view ('selamat', ['data'->$request]);
    }
}
